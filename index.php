<?php
  include('backend/bd/conexion.php');
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
     <link rel="stylesheet" href="css/estilos.css"> 
</head>

<style>
  .image-upload>input {
  display: none;
}
</style>
<nav class="navbar navbar-expand-lg navbar-dark bg-info fixed-top">
      <a class="navbar-brand" href="index.php">TE LO REGALO</a>
      <button class="navbar-toggler" data-target="#my-nav" data-toggle="collapse" aria-controls="my-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>
      <div id="my-nav" class="collapse navbar-collapse">
          <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                  <a class="nav-link" href="#">Catalogo</a>
              </li>
             <?php 
             if(isset($_SESSION['rut']) && $_SESSION['rut']==="1" ){ ?>
              <li class="nav-item">
                  <a class="nav-link active" href="#" >Administracion</a>
              </li>
              <?php } ?>
              <li class="nav-item ml-5" style="width: 400px;">
                  <div class="input-group">
                  <input type="text" class="form-control" placeholder="Buscar..." aria-label="Recipient's username" aria-describedby="button-addon2">
                  <div class="input-group-append">
                  <button class="btn btn-secondary" type="button" id="button-addon2">Buscar</button>
                   </div>
                 </div> 
              </li>
          </ul>

         
          <ul class="navbar-nav ml-auto">
            <?php 
                if(!isset($_SESSION['rut'])){
            ?>
              <li class="nav-item active">
                  <a class="nav-link" href="login.php">Ingresar</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link active" href="registro.php">Registro</a>
              </li>
                <?php }else{ ?>
                    <li class="nav-item dropdown" style="margin-right:100px;">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <?=strstr($_SESSION['user'],' ',true);?>
                    <?php }?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                   <form action="backend/modelos/logout.php" method="get">
                   <button class="dropdown-item" type="submit">Cerrar Sesion</button>
                   </form>
                </li>
          </ul>
      </div>
  </nav>
<div class="container mt-5 text-center">
    <div class="row">
        <div class="col-md-6 offset-3 mt-5">
         <div class="card">
             <div class="card-header bg-info text-white">
                 <b>Crear publicacion</b> 
             </div>
             <div class="card-body">
                     <div class="form-group">
                         <textarea id="text" name="text" class="form-control" name="" rows="3"></textarea>
                     </div>     
                     <div class="form-group" >
                         <div class="row">
                            <div class="col-md-6">
                                Agregar a tu publicacion
                              </div>
                              <div class="col-md-6">
                                  <div class="image-upload">
                                  <label for="file-input">
                                      <span class="btn btn-outline-primary"><i class="fa fa-image"></i></span>
                                    </label>
                                    <input id="file-input" type="file" accepts="image/*" />
                                    <label for="file-input">
                                      <span class="btn btn-outline-primary"><i class="fa fa-map"></i></span>
                                    </label>
                                    <input id="file-input" type="file" />
                                    <label for="file-input">
                                      <span class="btn btn-outline-primary"><i class="fa fa-users"></i></span>
                                    </label>
                                    <input id="file-input" type="file" />
                                  </div>
                              </div>
                         </div>
                       
                     </div>
                 <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Publicar</button>
                 </div>
             </div>
         </div>
        </div>
        <div class="col-md-6 offset-3 mt-3">
        <?php
          if(isset($_SESSION['rut'])){
              $consulta="SELECT * FROM users";
              $query= mysqli_query($conexion,$consulta);
         
              while($row = mysqli_fetch_array($query)){
                  ?>
                  <div class="card mt-4">
                      <div class="card-header">
                     <?=$row['nombre'];?>
                      </div>
                      <div class="card-body">
                          <?=$row['email'];?>
                      </div>
                  </div>
               <?php   
              }
          }
        ?>
    </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
