<?php
  include('backend/bd/conexion.php');
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
     <link rel="stylesheet" href="css/estilos.css"> 
</head>
<nav class="navbar navbar-expand-lg navbar-dark bg-info fixed-top">
      <a class="navbar-brand" href="index.php">TE LO REGALO</a>
      <button class="navbar-toggler" data-target="#my-nav" data-toggle="collapse" aria-controls="my-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>
      <div id="my-nav" class="collapse navbar-collapse">
          <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                  <a class="nav-link" href="#">Item 1</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link active" href="#" >Item 2</a>
              </li>
              <li class="nav-item ml-5" style="width: 400px;">
                  <div class="input-group">
                  <input type="text" class="form-control" placeholder="Buscar..." aria-label="Recipient's username" aria-describedby="button-addon2">
                  <div class="input-group-append">
                  <button class="btn btn-secondary" type="button" id="button-addon2">Buscar</button>
                   </div>
                 </div> 
              </li>
          </ul>
          <ul class="navbar-nav ml-auto">
            <?php 
                if(!isset($_SESSION['rut'])){
            ?>
              <li class="nav-item active">
                  <a class="nav-link" href="login.php">Ingresar</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link active" href="registro.php">Registro</a>
              </li>
                <?php }else{ ?>
                    <li class="nav-item dropdown" style="margin-right:100px;">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <?=strstr($_SESSION['rut'],' ',true);?>
                    <?php }?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <form action="backend/modelos/logout.php" method="get">
                   <button class="dropdown-item" type="submit">Cerrar Sesion</button>
                   </form>
                </li>
          </ul>
      </div>
  </nav>
<div class="container" style="padding-top:100px;">
    <div class="col-md-7 offset-2" style="padding-left:50px;">
        <div class="card">
            <div class="card-header bg-info text-white text-center">
                <h3>Login</h3>
            </div>
            <div class="card-body">
            <?php
                if(isset($_SESSION['mensaje'])){
                ?> 
                <div class="alert alert-success" >
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                 <?php
                 echo $_SESSION['mensaje'];
                 session_unset();
                 echo "</div>";
                }            
                ?>    
              <form action="backend/modelos/logear.php" method="post">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" class="form-control" >
                </div>
                <div class="form-group">
                    <label for="pass">Contraseña</label>
                    <input type="password" class="form-control" name="pass" id="pass">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-block btn-lg btn-outline-info">Entrar</button>
                </div>
                </form>
            </div>
        </div>

    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
