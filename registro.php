<?php
  include('backend/bd/conexion.php');
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
     <link rel="stylesheet" href="../css/estilos.css"> 
</head>
<nav class="navbar navbar-expand-lg navbar-dark bg-info fixed-top">
      <a class="navbar-brand" href="index.php">TE LO REGALO</a>
      <button class="navbar-toggler" data-target="#my-nav" data-toggle="collapse" aria-controls="my-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>
      <div id="my-nav" class="collapse navbar-collapse">
          <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                  <a class="nav-link" href="#">Item 1</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link active" href="#" >Item 2</a>
              </li>
              <li class="nav-item ml-5" style="width: 400px;">
                  <div class="input-group">
                  <input type="text" class="form-control" placeholder="Buscar..." aria-label="Recipient's username" aria-describedby="button-addon2">
                  <div class="input-group-append">
                  <button class="btn btn-secondary" type="button" id="button-addon2">Buscar</button>
                   </div>
                 </div> 
              </li>
          </ul>

         
          <ul class="navbar-nav ml-auto">
            <?php 
                if(!isset($_SESSION['rut'])){
            ?>
              <li class="nav-item active">
                  <a class="nav-link" href="login.php">Ingresar</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link active" href="registro.php">Registro</a>
              </li>
                <?php }else{ ?>
                    <li class="nav-item dropdown" style="margin-right:100px;">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <?=strstr($_SESSION['rut'],' ',true);?>
                    <?php }?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <form action="backend/modelos/logout.php" method="get">
                   <button class="dropdown-item" type="submit">Cerrar Sesion</button>
                   </form>
                </li>
          </ul>
      </div>
  </nav>
<div class="container" style="padding-top:100px;">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-info text-white text-center">
                <h3>Registrar</h3>
            </div>
            <div class="card-body">
            <?php
                if(isset($_SESSION['mensaje'])){
                ?>
                <div class="alert alert-info">
                 <?php
                   $_SESSION['mensaje'];
                   session_unset();
                   echo "</div>";
                }
                ?>
           <form action="backend/modelos/registrar.php" method="post">
             <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                    <label for="rut">Rut</label>
                    <input type="text" class="form-control" id="rut"  required oninput="checkRut(this)" name="rut" placeholder="Ingrese su RUT, ej: 11111111-1" >
                </div>
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" id="nombre" placeholder="Ingrese su nombre completo" class="form-control">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" class="form-control" placeholder="Ingrese su coreo electronico">
                </div>
                <div class="form-group">
                    <label for="direccion">Direccion</label>
                    <input type="text" name="direccion" id="direccion" class="form-control" placeholder="Ingrese su direccion">
                </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="nac">Fecha de nacimiento</label>
                        <input type="date" class="form-control" id="nac" name="nac"  >
                    </div>
                    <div class="form-group">
                     <label for="pass">Contraseña</label>
                     <input type="password" class="form-control" id="pass" name="pass">
                    </div>
                    <div class="form-group">
                        <label for="passc">Confirmar Contraseña</label>
                        <input type="password" class="form-control" id="passc" name="passc">
                       </div>
                    <div class="form-group">
                        <label for="sexo">Sexo</label>
                        <select name="sexo" id="sexo" class="form-control">
                            <option value="M">Masculino</option>
                            <option value="F">Femenino</option>
                            <option value="O">Otro</option>
                        </select>
                    </div>
                   
                </div>
            </div>

            <div class="col-md-6 offset-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <button type="submit" name="registrar" id="registrar" class="btn btn-block btn-lg btn-outline-info">Registrar</button>
                </div>
            </div>
                </form>
            </div>
        </div>

    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/inputmask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
<!--<script src="js/validarRut.js"></script>
